# Changelog
Tous les changements notables apportés à ce projet seront documentés dans ce fichier.

Le format est basé sur [Keep a Changelog](http://keepachangelog.com/fr/1.0.0/)
et ce projet adhère à [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2018-04-24

### Ajouts
- Code source initial
- Documentation





